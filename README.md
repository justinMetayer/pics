# Pics App
## Liens utiles
- [Lien integration gitlab et heroku](https://medium.com/@JohanPujol/int%C3%A9gration-continue-d%C3%A9ploiement-continue-ci-cd-avec-gitlab-et-heroku-b809801a1524)
- [Lien vers gitmoji permettant de rendre plus agréable les commits](https://gitmoji.carloscuesta.me/)
- [Lien pour la génération automatique du changelog](https://github.com/frinyvonnick/gitmoji-changelog/blob/master/DOCUMENTATION.md)
- [Lien vers Husky pour l'automatisation de script (gitmoji-changelog)](https://github.com/typicode/husky)

## Description
Application **NodeJs** et **ReactJS** permettant d'afficher suite à une recherche les images de [Unsplash](https://unsplash.com/).

## Start
Permet de démarrer l'application sur le **http://localhost:3000**.
```bash
    npm install 
    npm start
```
