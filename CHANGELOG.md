# Changelog

<a name="0.1.0"></a>
## 0.1.0 (2020-03-13)

### Added

- 👷‍♂️ Adding Recette branch support [[b11d99d](https://gitlab.com/justinMetayer/pics/commit/b11d99dc5fa029733d27babf09e5c187aeedfa5c)]
- ✨ Add Husky for automate gitmoji-changelog [[f6225d2](https://gitlab.com/justinMetayer/pics/commit/f6225d2a6a7fe7f220653b83a98b998db424d07c)]
- ✨ Add gitlab-ci and config [[5ea006d](https://gitlab.com/justinMetayer/pics/commit/5ea006dce0af93d3aef391b2586f2d7e32a04ef9)]
- 🎉 Initial Commit [[a0f04a1](https://gitlab.com/justinMetayer/pics/commit/a0f04a16d43c48c0ced9a2b8ee0b2b305bced3b4)]

### Changed

- 🎨 warning: LF will be replaced by CRLF in .idea/inspectionAjout gitlab-ci et configuration [[657fbcd](https://gitlab.com/justinMetayer/pics/commit/657fbcd3b81d12f1f9359f265b13277e1f3cd596)]

### Fixed

- 🐛 Fix dependencies for publish note [[ebf9097](https://gitlab.com/justinMetayer/pics/commit/ebf9097431956f59bb43c786f86a2068906534d4)]
- 🐛 Fixing yaml for gitlab-ci [[c7746dc](https://gitlab.com/justinMetayer/pics/commit/c7746dcde1bedafc3cf59051003a8309ff3f8a34)]
- 🐛 Docker ruby version for deploy to heroku [[a91fbcd](https://gitlab.com/justinMetayer/pics/commit/a91fbcd333a39ab27811d4f9a9cc1a0b4e5051ab)]
- 🐛 Bug key for heroku by deploy stage [[78f2854](https://gitlab.com/justinMetayer/pics/commit/78f28545a7a97e552c9dbe9bcd8fccd2d58c7909)]
- 🐛 Changelog last push [[7228f59](https://gitlab.com/justinMetayer/pics/commit/7228f59862871440c0a8580d2ef8b8cadd682066)]
- 🐛 Changelog post generation bug [[0b4f38b](https://gitlab.com/justinMetayer/pics/commit/0b4f38b8f3ba8de6ea9554e5bfc4d9e0e62b7248)]
- 🐛 Changelog bug merge [[db95d18](https://gitlab.com/justinMetayer/pics/commit/db95d18f49c6e5c60302082a0fd27b58c066fd1a)]
- 🐛 Bad generation of changelog [[9935b3b](https://gitlab.com/justinMetayer/pics/commit/9935b3b4cd5a9897783aa56561bb21ea4f6db6ae)]
- 🐛 Change configuration of husky [[278f4c2](https://gitlab.com/justinMetayer/pics/commit/278f4c298da89d3a8f15bb044f1511802432a68a)]
- 💚 Yaml structure correction [[2f1686f](https://gitlab.com/justinMetayer/pics/commit/2f1686f1c8c6e3ab2e6478531d0f6bb28ef65aff)]

### Miscellaneous

- 🚩 Update Automatic publish release notes [[6461191](https://gitlab.com/justinMetayer/pics/commit/64611916f811d58eeab8dc7f2ac4d08813e3728e)]
- 🚩 Add auto release note [[96e183c](https://gitlab.com/justinMetayer/pics/commit/96e183cb3f290f70e4e891f5babd46ebc7b38169)]
- 🚩 Finish feature gitlab-ci [[f022cda](https://gitlab.com/justinMetayer/pics/commit/f022cda382300e45dcf326f67411cc764b86b020)]
- 🚩 Change changelog [[7e5b6c4](https://gitlab.com/justinMetayer/pics/commit/7e5b6c4c0e6cd5a71ee23bee88d94cb2b097c2c1)]
- 🚩 Change changelog [[48afdd2](https://gitlab.com/justinMetayer/pics/commit/48afdd2a864b037b6cc4d8ab50db5f8ad7f04bb7)]
- 📝 Add link gitmoji-changelog and husky [[6c0c12e](https://gitlab.com/justinMetayer/pics/commit/6c0c12e8e53264b7e89b66ed58cacb660729e5a5)]
- 📝 Add link gitmoji [[0e9856f](https://gitlab.com/justinMetayer/pics/commit/0e9856f98f2bf54617d5d37a17e0018721904407)]
- 📝 Change README [[107a3a8](https://gitlab.com/justinMetayer/pics/commit/107a3a8adf00517370b581c0e437c0a0bfd9644e)]


